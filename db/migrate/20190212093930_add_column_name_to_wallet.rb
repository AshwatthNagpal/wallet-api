class AddColumnNameToWallet < ActiveRecord::Migration[5.2]
  def change
    add_column :wallets, :name, :string
  end
end
