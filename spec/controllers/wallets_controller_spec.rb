require 'rails_helper'

describe WalletsController do
  describe '#deposit' do
    let(:wallet) {FactoryBot.create(:wallet)}

    it 'expects to deposit ' do
      post :deposit, params: {id: wallet.id, amount: "100.0"}
      expect(response).to be_ok
      wallet.reload
      expect(wallet.balance).to eq(100.0)
      actual = JSON.parse(response.body)
      expect(actual['balance']).to eq(wallet.balance)
      expect(actual['id']).to eq(wallet.id)
    end
    it 'should return error for negative amount' do
      post :deposit, params: {id: wallet.id, amount: -100}
      expect(response).to be_bad_request
      actual = JSON.parse(response.body)
      expect(actual['error']).to eq('invalid amount')
    end
  end

  describe '#withdraw' do
    let(:wallet) {FactoryBot.create(:wallet, balance: 200)}

    it 'expects to withdraw ' do
      post :withdraw, params: {id: wallet.id, amount: "100.0"}
      expect(response).to be_ok
      wallet.reload
      expect(wallet.balance).to eq(100.0)
      actual = JSON.parse(response.body)
      expect(actual['balance']).to eq(wallet.balance)
      expect(actual['id']).to eq(wallet.id)
    end

    it 'should return error for negative amount' do
      post :withdraw, params: {id: wallet.id, amount: -100}
      expect(response).to be_bad_request
      actual = JSON.parse(response.body)
      expect(actual['error']).to eq('invalid amount')
    end

    it 'should return error if amount greater than balance is withdraw' do
      post :withdraw, params: {id: wallet.id, amount: 300}
      expect(response).to be_bad_request
      actual = JSON.parse(response.body)
      expect(actual['error']).to eq('insufficient balance')
    end
  end
end