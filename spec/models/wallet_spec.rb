require 'rails_helper'
RSpec::Matchers.define_negated_matcher :not_change, :change
describe Wallet do
  let(:wallet) {create(:wallet, balance: 300)}

  it {should validate_presence_of(:name)}
  it {should have_many(:transactions)}


  # it 'shouild not allow creating a wallet with no name' do
  #   wallet = Wallet.new({balance: 100, name: nil})
  #   expect(wallet.save).to be(false)
  # end

  describe '#deposit' do
    it 'should deposit a positive amount' do
      expect {wallet.deposit(100)}.to change {wallet.balance}.by 100
    end
    it 'should not deposit a negative amount' do
      expect {wallet.deposit(-1)}
          .to not_change {wallet.balance}
                  .and raise_error
    end

    it 'should add transaction for depositing amount ' do
      expect {wallet.deposit(100)}.to change {wallet.transactions.size}.by 1
    end
  end

  describe '#withdraw' do
    it 'should withdraw a positive amount' do
      expect {wallet.withdraw(100)}.to change {wallet.balance}.by -100
    end

    it 'should not change the wallet and raise an error when withdrawing a negative amount' do
      expect {wallet.withdraw(-100)}.to not_change {wallet.balance}.and raise_error('invalid amount')
    end

    it 'should not withdraw amount greater than balance' do
      expect {wallet.withdraw(400)}.to not_change {wallet.balance}.and raise_error('insufficient balance')
    end

    it 'should add transaction for depositing amount ' do
      wallet.withdraw(100)
      expect(wallet.transactions.count).to be 1
      expect(wallet.transactions.first.amount).to eq(-100)
      expect(wallet.transactions.first.wallet_id).to be 1
    end
  end
end