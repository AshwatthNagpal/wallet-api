require 'rails_helper'

describe Transaction do
  describe '#add_transaction' do
    let(:wallet) {create(:wallet)}

    let(:transaction) {create(:transaction, amount: 0, wallet: wallet)}

    it {should validate_presence_of(:amount)}

    it {should validate_presence_of(:wallet_id)}

    it {should belong_to(:wallet)}

  end
end