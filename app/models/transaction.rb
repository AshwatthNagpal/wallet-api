class Transaction < ApplicationRecord
  belongs_to :wallet
  validates :amount, presence: true
  validates :wallet_id, presence: true


end