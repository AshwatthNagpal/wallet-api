class Wallet < ApplicationRecord
  has_many :transactions
  validates :name, presence: true

  def deposit(amount)
    raise 'invalid amount' if amount < 0
    self.balance += amount
    transactions.create(amount: amount, wallet: self)
    save
  end

  def withdraw(amount)
    raise 'invalid amount' if amount < 0
    raise 'insufficient balance' if amount > self.balance
    self.balance -= amount
    transactions.create(amount: -amount, wallet: self)
    save
  end

end